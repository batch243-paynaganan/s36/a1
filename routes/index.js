// dependencies
const express=require('express');
const mongoose=require('mongoose');
// app using express function
const app=express();
const port=3001;
const taskRoute = require("./taskRoute")
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/tasks", taskRoute)
mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.e2dwbki.mongodb.net/s36-activity")

app.listen(port,()=>console.log(`Server running at port ${port}`))