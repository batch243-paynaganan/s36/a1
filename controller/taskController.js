const task = require("../models/task")
const Task=require("../models/task")
module.exports.getAllTasks=()=>{
    return Task.find({}).then(result=>{
        return result
    })
}
module.exports.createTask=(requestBody)=>{
    let newTask=new Task({
        name: requestBody.name
    })

return newTask.save().then((task,error)=>{
    if(error){
        console.log(error)
        return false;
    }else{
        return task;
    }
})}
module.exports.updateTask=(taskId, newContent)=>{
    return Task.findById(taskId).then((result,error)=>{
        if(error){
            console.log(error)
            return false
        }
        result.status=newContent.status;
        return result.save().then((updatedTask,saveErr)=>{
            if(saveErr){
                console.log(saveErr)
                return false
            }else{
                return updatedTask
            }
        })})}
